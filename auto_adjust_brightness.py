#!/usr/local/bin/python

import time
from math import ceil
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
pin_to_circuit = 7


def calculate_expected_screen_brightness(lux_reading):
    """calculate the brightness, given the LDR reading."""
    max_lux = 10000
    min_lux = 4000

    max_screen_brightness = 255
    min_screen_brightness = 5

    adjusted_lux_reading = max(min_lux, min(lux_reading, max_lux))

    print lux_reading
    print adjusted_lux_reading

    if min_lux == adjusted_lux_reading:
        return max_screen_brightness
    elif max_lux == adjusted_lux_reading:
        return min_screen_brightness

    perc = 1 - (adjusted_lux_reading / float(max_lux))
    return int(ceil(perc * max_screen_brightness))


def change_screen_brightness(brightness):
    """change brightness of the RPI display."""
    with open('/sys/class/backlight/rpi_backlight/brightness', 'r+') as f:
        f.read()
        f.seek(0)
        f.write(str(brightness))
        f.close()


def rc_time(pin_to_circuit):
    """monitor light sensitivity."""
    reading = 0
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    time.sleep(5)
    GPIO.setup(pin_to_circuit, GPIO.IN)
    while GPIO.input(pin_to_circuit) == GPIO.LOW:
        reading += 1
    brightness = calculate_expected_screen_brightness(reading)
    print 'reading = %d      brightness = %d' % (reading, brightness)
    change_screen_brightness(brightness)


try:
    while True:
        rc_time(pin_to_circuit)
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
